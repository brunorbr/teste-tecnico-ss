# Dummy API Test

Os testes foram desenvolvidos em Javascript, com apoio dos Frameworks Mocha e Chai.

URL da API:  
http://dummy.restapiexample.com/

## Como Executar:

Para executar os testes, clone o repositório na sua máquina e execute o comando:

```
npm install
```
E à seguir para rodar os testes execute o comando:
```
npm test
```

Os resultados dos testes ficaram disponíveis em relatório no diretório:
```
mochawesome-report\assets\mochawesome.html
```

### Issues encontradas durante o desenvolvimento dos testes automatizados:

Apesar de existir a URL na documentação da API, o método PUT não parece funcionar, testes pelo Postman também apresentaram retornos inconclusivos.