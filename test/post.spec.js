import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const request = chai.request.agent('http://dummy.restapiexample.com/api/v1');
const expect = chai.expect;

describe('post', () => {
    context('Given I register a new employee', ()=> {
        it('should return status 200 and the new user entry', (done) => {
            let employee = {"name":"Daenerys Targaryen",
            "salary":"35000",
            "age":"14"
            };
            request
                .post('/create')
                .send(employee)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body.data.name).to.be.equal(employee.name);
                    expect(res.body.data.salary).to.be.equal(employee.salary);
                    expect(res.body.data.age).to.be.equal(employee.age);
                    done();
                });
        });
        
        it("should return an error upon a post without a name", (done) => {
            let missingNameEmployee = {"salary":"3200", "age": "48"};
            request
                .post('/create')
                .send(missingNameEmployee)
                .end((err, res) => {
                    expect(res.body).to.not.have.status(200);
                    done();
                })
        
        })
    })
})