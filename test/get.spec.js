import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const request = chai.request.agent('http://dummy.restapiexample.com/api/v1');
const expect = chai.expect;

describe('get', () => {
    context('Given I want to search for all employees', ()=> {
        it("should return an array of users", (done)=> {
            request
                .get('/employees')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body.data).to.be.an('array');
                    done();
            });
        })
    });

    context('Given I want to find an specific employee by its ID' ,()=>{
        it("should return an specific user when looking for ID", (done) => {
            request
                .get('/employee/2')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body.data.employee_name).to.be.equal("Garrett Winters");                    
                    expect(res.body.data.employee_salary).to.be.equal("170750");                    
                    expect(res.body.data.employee_age).to.be.equal("63");
                    done();                    
                });
            })
        })
    }
);