import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const request = chai.request.agent('http://dummy.restapiexample.com/api/v1');
const expect = chai.expect;

describe('delete', () => {

    let dataToBeDeleted = {
        "name":"Garth Greenhand",
        "salary":"12500",
        "age":"35"
    };

    let id;

    before((done) => {
        request
            .post("/create")
            .send(dataToBeDeleted)
            .end((err, res) =>{
                id = res.body.data.id;
                done();
            });
    })



    context('"Given I need to delete employee data"', (done)=> {
        



        it("should delete and display a success message", (done) =>{
        request
            .delete(`/delete/${id}`)
            .end((err, res) => {
                expect(res.body.message).to.be.equal("successfully! deleted Records")
                done();
            });
        })

        it("should return a 404 status if the ID doesn't exist", (done)=>{
            request
                .delete('delete/999')
                .end((err, res)=> {
                    expect(res).to.have.status(404)
                    done();
                });
        });
    })
});