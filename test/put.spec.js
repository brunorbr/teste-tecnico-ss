import chai from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);

const request = chai.request.agent('http://dummy.restapiexample.com/api/v1');
const expect = chai.expect;

describe('put', () => {

    let dataToBeUpdated = {
        "name":"Argilac Durrandon",
        "salary":"7000",
        "age":"45"
    };

    let id;



    context("Given I need to update an employee data", ()=> {

        before(done => {
            request
                .post("/create")
                .send(dataToBeUpdated)
                .end((err, res) =>{
                    id = res.body.data.id;
                    done();
                });
        });

        it("should return status 200 and update data", (done) => {
            dataToBeUpdated.name = "Orys Baratheon";
            dataToBeUpdated.age = "21";
            request
                .put(`/update/${id}`)
                .send(dataToBeUpdated)
                .end((err, res)=>{
                    expect(res).to.have.status(200);
                    expect(res.body.data.name).to.be.equal(dataToBeUpdated.name);
                    expect(res.body.data.salary).to.be.equal(dataToBeUpdated.salary);
                    expect(res.body.data.age).to.be.equal(dataToBeUpdated.age);
                    done();
                });
        })

        it("should return status 404 for an inexisting ID", done => {
            request
                .put(`/update/999`)
                .end((err, res)=>{
                    expect(res).to.have.status(404);
            });
        })
    })
});
